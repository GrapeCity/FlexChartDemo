﻿using C1.Win.Chart;
using FlexChartExplorer.Samples;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace FlexChartExplorer
{
    public partial class MainForm : Form
    {
        #region const
        Color MENU_BACK_COLOR = System.Drawing.Color.FromArgb(249, 249, 249);
        Color MENU_ONOVER_COLOR = System.Drawing.Color.FromArgb(21, 164, 250);
        Color MENU_SELECTED_COLOR = System.Drawing.Color.FromArgb(230, 230, 230);
        int MENU_BUTTON_HEIGHT = 30;
        #endregion

        Image _menuButtonImage;

        string[] menuButtons =
        {
            "介绍", "绑定", "系列绑定", "页眉和页脚",
            "选择", "标签", "命中测试", "缩放", "气泡图",
            "金融图表", "多轴", "分区", "符号颜色", "趋势线",
            "饼图", "饼图选择"
        };
        public MainForm()
        {
            InitializeComponent();

            InitializeMenu();
        }

        private void InitializeMenu()
        {
            panelLinks.BackColor = MENU_BACK_COLOR;

            var top = 20;
            foreach (var item in menuButtons)
            {
                var newButton = new Button()
                {
                    Text = item,
                    Font = new Font("Calibri", 9, FontStyle.Bold),
                    ForeColor = Color.FromArgb(64, 64, 64),
                    TextAlign = ContentAlignment.MiddleLeft,
                    Width = panelLinks.Width - 2,
                    Height = MENU_BUTTON_HEIGHT,
                    Top = top,
                    FlatStyle = FlatStyle.Flat,
                    Left = 1
                };
                top += MENU_BUTTON_HEIGHT;
                newButton.FlatAppearance.BorderSize = 0;
                newButton.FlatAppearance.MouseOverBackColor = MENU_ONOVER_COLOR;
                newButton.Padding = new Padding(15, 0, 0, 0);
                newButton.Click += button1_Click;
                panelLinks.Controls.Add(newButton);

                //Init button style image
                _menuButtonImage = new Bitmap(panelLinks.Width + 15, MENU_BUTTON_HEIGHT + 1);
                using (Graphics g = Graphics.FromImage(_menuButtonImage))
                {
                    g.Clear(MENU_SELECTED_COLOR);
                    g.FillRectangle(new SolidBrush(MENU_ONOVER_COLOR), 0, 0, 7, MENU_BUTTON_HEIGHT);
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            //Open first link
            if (panelLinks.Controls[0] is Button)
                (panelLinks.Controls[0] as Button).PerformClick();
            this.btnDoc.MouseClick += tool_Click;
            this.btnVedio.MouseClick += tool_Click;
            this.btnForum.MouseClick += tool_Click;
            this.btnDown.MouseClick += tool_Click;
            this.btnPurchse.MouseClick += tool_Click;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var clickedButton = (sender as Button);
            panelMain.Controls.Clear();
            Control newControl;

            var str = clickedButton.Text;
            switch (str)
            {
                case "介绍": newControl = new Introduction(); break;
                case "绑定": newControl = new FlexChartExplorer.Samples.Binding(); break;
                case "系列绑定": newControl = new SeriesBinding(); break;
                case "页眉和页脚": newControl = new HeaderAndFooter(); break;
                case "选择": newControl = new Selection(); break;
                case "标签": newControl = new Labels(); break;
                case "命中测试": newControl = new HitTest(); break;
                case "缩放": newControl = new Zoom(); break;
                case "气泡图": newControl = new Bubble(); break;
                case "金融图表": newControl = new FinancialChart(); break;
                case "多轴": newControl = new Axes(); break;
                case "分区": newControl = new Zones(); break;
                case "符号颜色": newControl = new SymbolColor(); break;
                case "趋势线": newControl = new FlexChartExplorer.Samples.TrendLine(); break;
                case "饼图": newControl = new FlexPieIntroduction(); break;
                case "饼图选择": newControl = new FlexPieSelection(); break;
                default: newControl = new Introduction(); break;
            }
            newControl.Dock = DockStyle.Fill;

            panelMain.Controls.Add(newControl);

            //Paint selection button color
            foreach (Button item in panelLinks.Controls)
                item.Image = null;

            clickedButton.Image = _menuButtonImage;
        }

        private void panelLinks_Paint(object sender, PaintEventArgs e)
        {
            using (var graphics = panelLinks.CreateGraphics())
            {
                graphics.Clear(MENU_BACK_COLOR);
                using (var pen = new Pen(new SolidBrush(Color.DarkGray)))
                {
                    graphics.DrawRectangle(pen, 0, 0, panelLinks.Width - 1, panelLinks.Height - 1);
                }
            }
        }

        private void tool_Click(object sender, EventArgs e)
        {
            var name = (sender as Button).Name;
            switch (name)
            {
                case "btnDoc": System.Diagnostics.Process.Start("http://www.gcpowertools.com.cn/support/document.htm?utm_source=Demo&utm_medium=componentone&utm_term=flexgriddemo&utm_content=document&utm_campaign=win"); break;
                case "btnVedio": System.Diagnostics.Process.Start("http://gcdn.gcpowertools.com.cn/showforum-157.aspx?utm_source=Demo&utm_medium=componentone&utm_term=flexgriddemo&utm_content=video&utm_campaign=win"); break;
                case "btnForum": System.Diagnostics.Process.Start("http://gcdn.gcpowertools.com.cn/showforum-66.aspx?utm_source=Demo&utm_medium=componentone&utm_term=flexgriddemo&utm_content=forum&utm_campaign=win"); break;
                case "btnDown": System.Diagnostics.Process.Start("http://www.gcpowertools.com.cn/products/download.aspx?pid=3&utm_source=Demo&utm_medium=componentone&utm_term=flexgriddemo&utm_content=download&utm_campaign=win"); break;
                case "btnPurchse": System.Diagnostics.Process.Start("http://www.gcpowertools.com.cn/order/price.htm?utm_source=Demo&utm_medium=componentone&utm_term=flexgriddemo&utm_content=purchase&utm_campaign=win"); break;

            }
        }
    }
}
