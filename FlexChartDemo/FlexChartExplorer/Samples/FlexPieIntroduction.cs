﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C1.Win.Chart;
using System.Drawing.Drawing2D;

namespace FlexChartExplorer.Samples
{
    public partial class FlexPieIntroduction : UserControl
    {
        public FlexPieIntroduction()
        {
            InitializeComponent();
            InitializeControls();

            //Draw chart
            SetupChart();
        }

        void SetupChart()
        {
            flexPie1.BeginUpdate();

            // Set x-binding and add data to the chart
            flexPie1.Binding = "April";
            flexPie1.BindingName = "Fruit";
            flexPie1.DataSource = DataCreator.CreateFruit();

            //Set labels source
            flexPie1.DataLabel.Content = "{y}";

            flexPie1.EndUpdate();
        }

        private void cbInnerRadius_SelectedIndexChanged(object sender, EventArgs e)
        {
            flexPie1.InnerRadius = double.Parse((cbInnerRadius.SelectedItem as ComboBoxItem).Text);
        }

        private void cbOffset_SelectedIndexChanged(object sender, EventArgs e)
        {
            flexPie1.Offset = double.Parse((cbOffset.SelectedItem as ComboBoxItem).Text);
        }

        private void cbStartAngle_SelectedIndexChanged(object sender, EventArgs e)
        {
            flexPie1.StartAngle = double.Parse((cbStartAngle.SelectedItem as ComboBoxItem).Text);
        }
        private void cbPalette_SelectedIndexChanged(object sender, EventArgs e)
        {
            flexPie1.Palette = (C1.Chart.Palette)Enum.Parse(typeof(C1.Chart.Palette), (cbPalette.SelectedItem as ComboBoxItem).Text);
        }
        private void cbLabels_SelectedIndexChanged(object sender, EventArgs e)
        {
            flexPie1.DataLabel.Position = (C1.Chart.PieLabelPosition)Enum.Parse(typeof(C1.Chart.PieLabelPosition), (cbLabels.SelectedItem as ComboBoxItem).Text);
            if (cbLabelsBorder != null)
                cbLabelsBorder.Visible = flexPie1.DataLabel.Position != C1.Chart.PieLabelPosition.None;
        }

        private void cbLabelsBorder_SelectedIndexChanged(object sender, EventArgs e)
        {
            flexPie1.DataLabel.Border = bool.Parse((cbLabelsBorder.SelectedItem as ComboBoxItem).Text);
        }

        private void InitializeControls()
        {
            #region Init controls

            baseSample1.pChart.Controls.Clear();
            flexPie1 = new FlexPie()
            {
                Dock = DockStyle.Fill,
                Visible = true,
                BackColor = Color.White,
            };

            baseSample1.pChart.Controls.Add(flexPie1);

            //Set localized text
            baseSample1.lblTitle.Text = Localizer.GetItem("flexpie", "title");
            baseSample1.tbDescription.Rtf = Localizer.GetItem("flexpie", "description").MakeRtf();

            #region ComboBox Inner radius
            cbInnerRadius = new ComboBox()
            {
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray
            };

            foreach (var item in new[] { 0, 0.25, 0.50, 0.75 })
                cbInnerRadius.Items.Add(new ComboBoxItem("Inner radius") { Text = item.ToString() });

            cbInnerRadius.SelectedIndex = 0;

            cbInnerRadius.SelectedIndexChanged += cbInnerRadius_SelectedIndexChanged;

            baseSample1.pControls.Controls.Add(cbInnerRadius);
            #endregion

            #region ComboBox Offset
            cbOffset = new ComboBox()
            {
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray
            };

            foreach (var item in new[] { 0, 0.1, 0.2, 0.3, 0.4, 0.5 })
                cbOffset.Items.Add(new ComboBoxItem("Offset") { Text = item.ToString() });

            cbOffset.SelectedIndexChanged += cbOffset_SelectedIndexChanged;

            cbOffset.SelectedIndex = 0;

            baseSample1.pControls.Controls.Add(cbOffset); 
            #endregion

            #region ComboBox Start angle
            cbStartAngle = new ComboBox()
            {
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray
            };

            foreach (var item in new[] { 0, 90, 180, -90 })
                cbStartAngle.Items.Add(new ComboBoxItem("Start angle") { Text = item.ToString() });

            cbStartAngle.SelectedIndexChanged += cbStartAngle_SelectedIndexChanged;

            cbStartAngle.SelectedIndex = 0;

            baseSample1.pControls.Controls.Add(cbStartAngle);
            #endregion

            #region ComboBox Palette
            cbPalette = new ComboBox()
            {
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray
            };

            foreach (var item in Enum.GetValues(typeof(C1.Chart.Palette)))
                cbPalette.Items.Add(new ComboBoxItem("Palette") { Text = ((C1.Chart.Palette)item).ToString(), Value = (int)item });
            
            cbPalette.SelectedIndexChanged += cbPalette_SelectedIndexChanged;
            
            cbPalette.SelectedIndex = 0;

            baseSample1.pControls.Controls.Add(cbPalette);

            #endregion

            #region ComboBox Labels

            cbLabels = new ComboBox()
            {
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray
            };

            foreach (var item in Enum.GetValues(typeof(C1.Chart.PieLabelPosition)))
                cbLabels.Items.Add(new ComboBoxItem("Labels") { Text = ((C1.Chart.PieLabelPosition)item).ToString(), Value = (int)item });

            cbLabels.SelectedIndexChanged += cbLabels_SelectedIndexChanged;

            cbLabels.SelectedIndex = 0;

            baseSample1.pControls.Controls.Add(cbLabels);

            #endregion

            #region ComboBox Labels border
            cbLabelsBorder = new ComboBox()
            {
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray,
                Visible = false
            };

            foreach (var item in new[] { "false", "true" })
                cbLabelsBorder.Items.Add(new ComboBoxItem("Labels border") { Text = item });

            cbLabelsBorder.SelectedIndex = 0;

            cbLabelsBorder.SelectedIndexChanged += cbLabelsBorder_SelectedIndexChanged;

            baseSample1.pControls.Controls.Add(cbLabelsBorder);
            #endregion

            #endregion
        }
    }
}
