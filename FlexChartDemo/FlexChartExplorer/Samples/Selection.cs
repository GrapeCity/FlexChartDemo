﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C1.Win.Chart;
using System.Drawing.Drawing2D;

namespace FlexChartExplorer.Samples
{
    public partial class Selection : UserControl
    {
        public Selection()
        {
            InitializeComponent();
            InitializeControls();

            SetupChart();
        }
        void SetupChart()
        {
            flexChart1.BeginUpdate();

            // Add data to the chart
            flexChart1.Series.Clear();

            var s1 = new Series();
            s1.Binding = s1.Name = "March";
            flexChart1.Series.Add(s1);

            var s2 = new Series();
            s2.Binding = s2.Name = "April";
            flexChart1.Series.Add(s2);

            var s3 = new Series();
            s3.Binding = s3.Name = "May";
            flexChart1.Series.Add(s3);

            flexChart1.BindingX = "Fruit";
            flexChart1.DataSource = DataCreator.CreateFruit();

            flexChart1.EndUpdate();
        }

        private void cbChartType_SelectedIndexChanged(object sender, EventArgs e)
        {
            flexChart1.ChartType = (C1.Chart.ChartType)Enum.Parse(typeof(C1.Chart.ChartType), (cbChartType.SelectedItem as ComboBoxItem).Text);
        }

        private void cbStacked_SelectedIndexChanged(object sender, EventArgs e)
        {
            flexChart1.Stacking = (C1.Chart.Stacking)Enum.Parse(typeof(C1.Chart.Stacking), (cbStacked.SelectedItem as ComboBoxItem).Text);
        }

        private void cbMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            flexChart1.SelectionMode = (C1.Chart.ChartSelectionMode)Enum.Parse(typeof(C1.Chart.ChartSelectionMode), (cbMode.SelectedItem as ComboBoxItem).Text);
        }

        private void InitializeControls()
        {
            #region Init controls

            //Set localized text
            baseSample1.lblTitle.Text = Localizer.GetItem("selection", "title");
            baseSample1.tbDescription.Rtf = Localizer.GetItem("selection", "description").MakeRtf();

            flexChart1 = baseSample1.flexChart1;

            //Init ChartType combobox
            cbChartType = new ComboBox()
            {
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray
            };
            cbChartType.SelectedIndexChanged += cbChartType_SelectedIndexChanged;

            //Init Stacked combobox
            cbStacked = new ComboBox()
            {
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray
            };
            cbStacked.SelectedIndexChanged += cbStacked_SelectedIndexChanged;

            //Init Palette combobox
            cbMode = new ComboBox()
            {
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray
            };
            cbMode.SelectedIndexChanged += cbMode_SelectedIndexChanged;

            baseSample1.pControls.Controls.Add(cbChartType);
            baseSample1.pControls.Controls.Add(cbStacked);
            baseSample1.pControls.Controls.Add(cbMode);
            #endregion

            foreach (var item in Enum.GetValues(typeof(C1.Chart.ChartType)))
                if (item.ToString() != "Bubble" && item.ToString() != "Candlestick" && item.ToString() != "HighLowOpenClose")
                cbChartType.Items.Add(new ComboBoxItem("Chart type") { Text = ((C1.Chart.ChartType)item).ToString(), Value = (int)item });
            cbChartType.SelectedIndex = 0;

            foreach (var item in Enum.GetValues(typeof(C1.Chart.Stacking)))
                cbStacked.Items.Add(new ComboBoxItem("Stacking") { Text = ((C1.Chart.Stacking)item).ToString(), Value = (int)item });
            cbStacked.SelectedIndex = 0;

            foreach (var item in Enum.GetValues(typeof(C1.Chart.ChartSelectionMode)))
                cbMode.Items.Add(new ComboBoxItem("Mode") { Text = ((C1.Chart.ChartSelectionMode)item).ToString(), Value = (int)item });
            cbMode.SelectedIndex = 1;
        }
    }
}
