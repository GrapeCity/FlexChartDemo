﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C1.Win.Chart;
using System.Drawing.Drawing2D;

namespace FlexChartExplorer.Samples
{
    public partial class Introduction : UserControl
    {
        public Introduction()
        {
            InitializeComponent();
            InitializeControls();

            SetupChart();
        }

        void SetupChart()
        {
            flexChart1.BeginUpdate();

            flexChart1.Series.Clear();

            // Add 3 data series
            var s1 = new Series();
            s1.Binding = s1.Name = "March";
            flexChart1.Series.Add(s1);

            var s2 = new Series();
            s2.Binding = s2.Name = "April";
            flexChart1.Series.Add(s2);

            var s3 = new Series();
            s3.Binding = s3.Name = "May";
            flexChart1.Series.Add(s3);

            // Set x-binding and add data to the chart
            flexChart1.BindingX = "Fruit";
            flexChart1.DataSource = DataCreator.CreateFruit();

            flexChart1.EndUpdate();
        }

        private void cbChartType_SelectedIndexChanged(object sender, EventArgs e)
        {
            flexChart1.ChartType = (C1.Chart.ChartType)Enum.Parse(typeof(C1.Chart.ChartType), (cbChartType.SelectedItem as ComboBoxItem).Text);
        }

        private void cbStacked_SelectedIndexChanged(object sender, EventArgs e)
        {
            flexChart1.Stacking = (C1.Chart.Stacking)Enum.Parse(typeof(C1.Chart.Stacking), (cbStacked.SelectedItem as ComboBoxItem).Text);
        }

        private void cbPalette_SelectedIndexChanged(object sender, EventArgs e)
        {
            flexChart1.Palette = (C1.Chart.Palette)Enum.Parse(typeof(C1.Chart.Palette), (cbPalette.SelectedItem as ComboBoxItem).Text);
        }

        private void cbRotated_CheckedChanged(object sender, EventArgs e)
        {
            flexChart1.Rotated = chbRotate.Checked;
        }

        private void InitializeControls()
        {

            #region Init controls

            //Set localized text
            baseSample1.lblTitle.Text = Localizer.GetItem("introduction", "title");
            baseSample1.tbDescription.Rtf = Localizer.GetItem("introduction", "description").MakeRtf();

            flexChart1 = baseSample1.flexChart1;

            //Init ChartType combobox
            cbChartType = new ComboBox()
            {
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray
            };
            cbChartType.SelectedIndexChanged += cbChartType_SelectedIndexChanged;

            //Init Palette combobox
            cbPalette = new ComboBox()
            {
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray
            };
            cbPalette.SelectedIndexChanged += cbPalette_SelectedIndexChanged;

            //Init Stacked combobox
            cbStacked = new ComboBox()
            {
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray
            };
            cbStacked.SelectedIndexChanged += cbStacked_SelectedIndexChanged;

            //Init Rotete chackbox
            chbRotate = new CheckBox()
            {
                Text = "Rotate",
                FlatStyle = FlatStyle.Flat,
                Checked = false
            };
            chbRotate.CheckedChanged += cbRotated_CheckedChanged;

            baseSample1.pControls.Controls.Add(cbChartType);
            baseSample1.pControls.Controls.Add(cbPalette);
            baseSample1.pControls.Controls.Add(cbStacked);
            baseSample1.pControls.Controls.Add(chbRotate);
            #endregion

            foreach (var item in Enum.GetValues(typeof(C1.Chart.ChartType)))
                if (item.ToString() != "Bubble" && item.ToString() != "Candlestick" && item.ToString() != "HighLowOpenClose")
                    cbChartType.Items.Add(new ComboBoxItem("Chart type") { Text = ((C1.Chart.ChartType)item).ToString(), Value = (int)item });
            cbChartType.SelectedIndex = 0;

            foreach (var item in Enum.GetValues(typeof(C1.Chart.Palette)))
                cbPalette.Items.Add(new ComboBoxItem("Palette") { Text = ((C1.Chart.Palette)item).ToString(), Value = (int)item });
            cbPalette.SelectedIndex = 0;

            foreach (var item in Enum.GetValues(typeof(C1.Chart.Stacking)))
                cbStacked.Items.Add(new ComboBoxItem("Stacking") { Text = ((C1.Chart.Stacking)item).ToString(), Value = (int)item });
            cbStacked.SelectedIndex = 0;
        }
    }
}
