﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C1.Win.Chart;
using System.Drawing.Drawing2D;

namespace FlexChartExplorer.Samples
{
    public partial class FlexPieSelection : UserControl
    {
        public FlexPieSelection()
        {
            InitializeComponent();
            InitializeControls();

            //Draw chart
            SetupChart();
        }

        void SetupChart()
        {
            flexPie1.BeginUpdate();

            // Set x-binding and add data to the chart
            flexPie1.Binding = "April";
            flexPie1.BindingName = "Fruit";
            flexPie1.DataSource = DataCreator.CreateFruit();

            //Set labels source
            flexPie1.DataLabel.Content = "{y}";
            flexPie1.SelectionMode = C1.Chart.ChartSelectionMode.Point;

            flexPie1.SelectionStyle.StrokeWidth = 3;

            flexPie1.EndUpdate();
        }

        private void cbSelectedPosition_SelectedIndexChanged(object sender, EventArgs e)
        {
            flexPie1.SelectedItemPosition = (C1.Chart.Position)Enum.Parse(typeof(C1.Chart.Position), (cbSelectedPosition.SelectedItem as ComboBoxItem).Text);
        }

        private void cbSelectedOffset_SelectedIndexChanged(object sender, EventArgs e)
        {
            flexPie1.SelectedItemOffset = double.Parse((cbSelectedOffset.SelectedItem as ComboBoxItem).Text);
        }


        private void cbLabelsBorder_SelectedIndexChanged(object sender, EventArgs e)
        {
            //flexPie1 = bool.Parse((cbIsAnimated.SelectedItem as ComboBoxItem).Text);
        }

        private void InitializeControls()
        {
            #region Init controls

            baseSample1.pDescription.Height = 50;
            baseSample1.pChart.Controls.Clear();
            flexPie1 = new FlexPie()
            {
                Dock = DockStyle.Fill,
                Visible = true,
                BackColor = Color.White,
            };

            baseSample1.pChart.Controls.Add(flexPie1);
            baseSample1.pControls.Visible = true;

            //Set localized text
            baseSample1.lblTitle.Text = Localizer.GetItem("flexpieselection", "title");
            baseSample1.tbDescription.Rtf = Localizer.GetItem("flexpieselection", "description").MakeRtf();

            #region ComboBox Selected position 
            cbSelectedPosition = new ComboBox()
            {
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray,
                Visible = true
            };

            foreach (var item in Enum.GetValues(typeof(C1.Chart.Position)))
                cbSelectedPosition.Items.Add(new ComboBoxItem("Selection mode") { Text = ((C1.Chart.Position)item).ToString(), Value = (int)item });
            cbSelectedPosition.SelectedIndexChanged += cbSelectedPosition_SelectedIndexChanged;

            cbSelectedPosition.SelectedIndex = 2;

            baseSample1.pControls.Controls.Add(cbSelectedPosition);
            #endregion

            #region ComboBox Selected item Offset
            cbSelectedOffset = new ComboBox()
            {
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray,
                Visible = true
            };

            foreach (var item in new[] { 0, 0.1, 0.2 })
                cbSelectedOffset.Items.Add(new ComboBoxItem("Selected item Offset") { Text = item.ToString() });

            cbSelectedOffset.SelectedIndexChanged += cbSelectedOffset_SelectedIndexChanged;

            cbSelectedOffset.SelectedIndex = 0;

            baseSample1.pControls.Controls.Add(cbSelectedOffset); 
            #endregion

            #region ComboBox cbIsAnimated
            cbIsAnimated = new ComboBox()
            {
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray,
                Visible = false
            };

            foreach (var item in new[] { "false", "true" })
                cbIsAnimated.Items.Add(new ComboBoxItem("Is animated") { Text = item });

            cbIsAnimated.SelectedIndex = 0;

            cbIsAnimated.SelectedIndexChanged += cbLabelsBorder_SelectedIndexChanged;

            baseSample1.pControls.Controls.Add(cbIsAnimated);
            #endregion

            #endregion
        }
    }
}
