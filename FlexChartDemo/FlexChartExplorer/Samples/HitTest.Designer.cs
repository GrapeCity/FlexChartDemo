﻿namespace FlexChartExplorer.Samples
{
    partial class HitTest
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private C1.Win.Chart.FlexChart flexChart1;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPosition2 = new System.Windows.Forms.Label();
            this.lblPosition = new System.Windows.Forms.Label();
            this.pInformation = new System.Windows.Forms.Panel();
            this.baseSample1 = new FlexChartExplorer.Samples.BaseSample();
            this.pInformation.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblPosition2
            // 
            this.lblPosition2.AutoSize = true;
            this.lblPosition2.Location = new System.Drawing.Point(200, 14);
            this.lblPosition2.Name = "lblPosition2";
            this.lblPosition2.Size = new System.Drawing.Size(0, 12);
            this.lblPosition2.TabIndex = 17;
            // 
            // lblPosition
            // 
            this.lblPosition.AutoSize = true;
            this.lblPosition.Location = new System.Drawing.Point(40, 14);
            this.lblPosition.Name = "lblPosition";
            this.lblPosition.Size = new System.Drawing.Size(0, 12);
            this.lblPosition.TabIndex = 16;
            // 
            // pInformation
            // 
            this.pInformation.Controls.Add(this.lblPosition2);
            this.pInformation.Controls.Add(this.lblPosition);
            this.pInformation.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pInformation.Location = new System.Drawing.Point(0, 444);
            this.pInformation.Name = "pInformation";
            this.pInformation.Size = new System.Drawing.Size(815, 65);
            this.pInformation.TabIndex = 4;
            // 
            // baseSample1
            // 
            this.baseSample1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.baseSample1.Font = new System.Drawing.Font("宋体", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.baseSample1.Location = new System.Drawing.Point(0, 0);
            this.baseSample1.Name = "baseSample1";
            this.baseSample1.Size = new System.Drawing.Size(815, 444);
            this.baseSample1.TabIndex = 5;
            // 
            // HitTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.baseSample1);
            this.Controls.Add(this.pInformation);
            this.Name = "HitTest";
            this.Size = new System.Drawing.Size(815, 509);
            this.pInformation.ResumeLayout(false);
            this.pInformation.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblPosition;
        private System.Windows.Forms.Label lblPosition2;
        private System.Windows.Forms.Panel pInformation;
        private BaseSample baseSample1;
    }
}
