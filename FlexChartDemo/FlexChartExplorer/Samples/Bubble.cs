﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C1.Win.Chart;
using System.Drawing.Drawing2D;
using C1.Chart;
using System.IO;

namespace FlexChartExplorer.Samples
{
    public partial class Bubble : UserControl
    {
        int npts = 100;
        Random rnd = new Random();

        public Bubble()
        {
            InitializeComponent();
            InitializeControls();

            SetupChart();
        }

        void SetupChart()
        {
            // Create sample data
            var data = new DataItem[npts];
            for (var i = 0; i < npts; i++)
            {
                data[i] = new DataItem() { X = rnd.NextDouble(), Y = rnd.NextDouble(), Size = rnd.Next(100) };
            }

            flexChart1.BeginUpdate();

            flexChart1.Series.Clear();

            // Add data to the chart
            flexChart1.Binding = "Y,Size";
            flexChart1.BindingX = "X";

            flexChart1.Series.Add(new Series());
            flexChart1.DataSource = data;

            flexChart1.ChartType = ChartType.Bubble;

            flexChart1.EndUpdate();
        }

        private void InitializeControls()
        {
            #region Init controls

            baseSample1.lblTitle.Text = Localizer.GetItem("bubble", "title");
            baseSample1.tbDescription.Rtf = Localizer.GetItem("bubble", "description").MakeRtf();
            
            flexChart1 = baseSample1.flexChart1;
            baseSample1.pDescription.Height = 130;
            baseSample1.pControls.Visible = false;

            #endregion
        }

        public class DataItem
        {
            public double X { get; set; }
            public double Y { get; set; }
            public double Size { get; set; }

        }
    }
}
