﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C1.Win.Chart;
using System.Drawing.Drawing2D;
using C1.Chart;
using System.IO;

namespace FlexChartExplorer.Samples
{
    public partial class HeaderAndFooter : UserControl
    {
        string[] year = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        Random rnd = new Random();

        public HeaderAndFooter()
        {
            InitializeComponent();
            InitializeControls();

            SetupChart();
        }

        void SetupChart()
        {
            // Create sample data
            var count = year.Count();
            var data = new DataItem[count-1];
            for (var i = 0; i < count-1; i++)
            {
                data[i] = new DataItem() { Amount = rnd.Next(0, 1000), Month = year[i] };
            }

            flexChart1.BeginUpdate();

            flexChart1.Series.Clear();

            // Add data to the chart
            flexChart1.Binding = "Amount";
            flexChart1.BindingX = "Month";

            flexChart1.Series.Add(new Series());

            flexChart1.DataSource = data;

            flexChart1.ChartType = ChartType.LineSymbols;

            // Add header and footer
            flexChart1.Header.Content = Localizer.GetItem("headerandfooter", "header");
            flexChart1.Footer.Content = Localizer.GetItem("headerandfooter", "footer");
            flexChart1.Header.Style.Font = flexChart1.Footer.Style.Font = new Font(FontFamily.GenericSansSerif, 15);
            flexChart1.Header.Style.FillColor = flexChart1.Footer.Style.FillColor = Color.DimGray;

            flexChart1.EndUpdate();
        }

        private void InitializeControls()
        {
            #region Init controls

            flexChart1 = baseSample1.flexChart1;
            baseSample1.lblTitle.Text = Localizer.GetItem("headerandfooter", "title");
            baseSample1.tbDescription.Rtf = Localizer.GetItem("headerandfooter", "description").MakeRtf();
            baseSample1.pDescription.Height = 90;
            baseSample1.pControls.Visible = false;

            #endregion
        }

        public class DataItem
        {
            public int Amount { get; set; }
            public string Month { get; set; }

        }
    }
}
