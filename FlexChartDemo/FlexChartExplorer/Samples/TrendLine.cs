﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C1.Win.Chart;
using System.Drawing.Drawing2D;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace FlexChartExplorer.Samples
{
    public partial class TrendLine : UserControl
    {
        C1.Win.Chart.TrendLine trendLine;
        ObservableCollection<DataItem> dataList = new ObservableCollection<DataItem>();
        DataItem clickedItem;

        public TrendLine()
        {
            InitializeComponent();

            flexChart1 = baseSample1.flexChart1;
            flexChart1.MouseDown += flexChart1_MouseDown;
            flexChart1.MouseMove += flexChart1_MouseMove;
            flexChart1.MouseUp += flexChart1_MouseUp;
            flexChart1.MouseLeave += flexChart1_MouseLeave;
            flexChart1.Rendered += flexChart1_Rendered;

            SetupChart();

            InitializeControls();
        }

        void flexChart1_Rendered(object sender, RenderEventArgs e)
        {
            if (trendLine == null) return;

            using (var brush = new SolidBrush(Color.LightGray))
            {
                var startPoint = new PointF(flexChart1.PlotRect.X + 10, flexChart1.PlotRect.Y + 10);
                e.Graphics.DrawString(GetFunction(trendLine), new Font(FontFamily.GenericSansSerif, 10, FontStyle.Regular), new SolidBrush(Color.Black), startPoint);
            }
        }

        void flexChart1_MouseLeave(object sender, EventArgs e)
        {
            clickedItem = null;
        }

        void flexChart1_MouseUp(object sender, MouseEventArgs e)
        {
            clickedItem = null;
        }

        void flexChart1_MouseMove(object sender, MouseEventArgs e)
        {
            if (clickedItem != null)
            {
                clickedItem.Y = (int)flexChart1.PointToData(new Point(e.X, e.Y)).Y;
                flexChart1.Rebind();
            }
        }

        void flexChart1_MouseDown(object sender, MouseEventArgs e)
        {
            var ht = flexChart1.HitTest(new Point(e.X, e.Y));
            if (ht.Distance < 3 && ht.X != null && ht.Y != null)
            {
                clickedItem = dataList.FirstOrDefault(x => x.X == Int32.Parse(ht.X.ToString()) && x.Y == Int32.Parse(ht.Y.ToString()));
            }
        }

        string GetFunction(C1.Win.Chart.TrendLine trendLine)
        {
            string result = String.Empty;
            int X = 1, Y0 = 0;

            switch (trendLine.FitType)
            {
                case C1.Chart.FitType.MaxX: result = "X=" + trendLine.GetValues(X).Max(); break;
                case C1.Chart.FitType.MinX: result = "X=" + trendLine.GetValues(X).Min(); break;
                case C1.Chart.FitType.MaxY: result = "Y=" + trendLine.GetValues(Y0).Max(); break;
                case C1.Chart.FitType.MinY: result = "Y=" + trendLine.GetValues(Y0).Min(); break;
                case C1.Chart.FitType.AverageX: result = "X=" + trendLine.GetValues(X).Average(); break;
                case C1.Chart.FitType.AverageY: result = "Y=" + trendLine.GetValues(Y0).Average(); break;
            }
            return result;
        }

        void SetupChart()
        {
            var rnt = new Random();

            for (int i = 0; i < 10; i++)
            {
                dataList.Add(new DataItem() { X = i, Y = rnt.Next(100) });
            }

            flexChart1.BeginUpdate();

            flexChart1.Series.Clear();

            flexChart1.ChartType = C1.Chart.ChartType.LineSymbols;

            var serie = new Series()
            {
                Name = "Base dataList",
                DataSource = dataList,
                Binding = "Y",
                BindingX = "X",
            };

            trendLine = new C1.Win.Chart.TrendLine()
            {
                Name = "Trend line",
                DataSource = dataList,
                Binding = "Y",
                BindingX = "X"
            };

            flexChart1.Series.Add(serie);
            flexChart1.Series.Add(trendLine);

            flexChart1.EndUpdate();
        }

        private void cbFitType_SelectedIndexChanged(object sender, EventArgs e)
        {
            trendLine.FitType = (C1.Chart.FitType)Enum.Parse(typeof(C1.Chart.FitType), (cbFitType.SelectedItem as ComboBoxItem).Text);
        }

        private void cbRotated_CheckedChanged(object sender, EventArgs e)
        {
            //flexChart1. = chbRotate.Checked;
        }

        private void InitializeControls()
        {

            #region Init controls

            //Set localized text
            baseSample1.lblTitle.Text = Localizer.GetItem("trendline", "title");
            var a = Localizer.GetItem("trendline", "description");
            baseSample1.tbDescription.Rtf = a.MakeRtf();
            //baseSample1.tbDescription.Text = a;
            //Init ChartType combobox
            cbFitType = new ComboBox()
            {
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray
            };
            cbFitType.SelectedIndexChanged += cbFitType_SelectedIndexChanged;

            //Init Rotete chackbox
            chbRotate = new CheckBox()
            {
                Text = "Rotate",
                FlatStyle = FlatStyle.Flat,
                Checked = false,
                Visible = false
            };
            chbRotate.CheckedChanged += cbRotated_CheckedChanged;

            baseSample1.pControls.Controls.Add(cbFitType);
            baseSample1.pControls.Controls.Add(chbRotate);
            #endregion

            foreach (var item in Enum.GetValues(typeof(C1.Chart.FitType)))
                cbFitType.Items.Add(new ComboBoxItem("Fit type") { Text = ((C1.Chart.FitType)item).ToString(), Value = (int)item });
            cbFitType.SelectedIndex = 0;
        }

        public class DataItem
        {
            public int X { get; set; }

            public int Y { get; set; }
        }
    }
}
