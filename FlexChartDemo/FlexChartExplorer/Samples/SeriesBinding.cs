﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C1.Win.Chart;
using System.Drawing.Drawing2D;

namespace FlexChartExplorer.Samples
{
    public partial class SeriesBinding : UserControl
    {
        public SeriesBinding()
        {
            InitializeComponent();
            InitializeControls();

            SetupChart();
        }

        void SetupChart()
        {
            flexChart1.BeginUpdate();

            flexChart1.Series.Clear();

            // Two series with independent data sources

            // 1st series
            Series function1 = new Series()
            {
                Name = "Function 1",
                DataSource = DataCreator.Create(x => 2 * Math.Sin(0.16 * x), y => 2 * Math.Cos(0.12 * y), 160),
                Binding = "YVals",
                BindingX = "XVals"
            };
            flexChart1.Series.Add(function1);

            // 2nd series 
            Series function2 = new Series()
            {
                Name = "Function 2",
                DataSource = DataCreator.Create(x => Math.Sin(0.1 * x), y => Math.Cos(0.15 * y), 160),
                Binding = "YVals",
                BindingX = "XVals"
            };
            flexChart1.Series.Add(function2);

            flexChart1.EndUpdate();
        }
        private void InitializeControls()
        {
            #region Init controls
            baseSample1.lblTitle.Text = Localizer.GetItem("seriesbinding", "title");
            baseSample1.tbDescription.Rtf = Localizer.GetItem("seriesbinding", "description").MakeRtf();

            flexChart1 = baseSample1.flexChart1;
            baseSample1.pDescription.Height = 220;

            flexChart1.AxisX.MajorGrid = false;
            flexChart1.ChartType = C1.Chart.ChartType.Line;
            baseSample1.pControls.Visible = false;

            #endregion
        }
    }
}
