﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C1.Win.Chart;
using System.Drawing.Drawing2D;

namespace FlexChartExplorer.Samples
{
    public partial class Binding : UserControl
    {
        int npts = 50;
        Random rnd = new Random();

        public Binding()
        {
            InitializeComponent();
            InitializeControls();

            SetupChart();
        }

        void SetupChart()
        {
            // Create sample data
            var data = new DataItem[npts];
            var dateStep = 0;
            for (var i = 0; i < npts; i++)
            {
                var date = DateTime.Today.AddDays(dateStep += 9);
                data[i] = new DataItem()
                {
                    Downloads = date.Month == 4 || date.Month == 8 ? (int?)null : rnd.Next(10, 20),
                    Sales = date.Month == 4 || date.Month == 8 ? (int?)null : rnd.Next(0, 10),
                    Date = date
                };
            }

            flexChart1.BeginUpdate();

            flexChart1.Series.Clear();

            // Add data to the chart
            flexChart1.BindingX = "Date";

            flexChart1.Series.Add(new Series() { Name = "Downloads", Binding = "Downloads" });
            flexChart1.Series.Add(new Series() { Name = "Sales", Binding = "Sales" });

            flexChart1.DataSource = data;

            flexChart1.EndUpdate();
        }

        private void cbChartType_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cbChartType.SelectedItem.ToString())
            {
                case "Chart type: Line": flexChart1.ChartType = C1.Chart.ChartType.Line; break;
                case "Chart type: LineSymbols": flexChart1.ChartType = C1.Chart.ChartType.LineSymbols; break;
                case "Chart type: Area": flexChart1.ChartType = C1.Chart.ChartType.Area; break;
            }
        }

        private void chbInterpolateNulls_CheckedChanged(object sender, EventArgs e)
        {
            flexChart1.Options.InterpolateNulls = chbInterpolateNulls.Checked;
        }

        private void chbLegendToggle_CheckedChanged(object sender, EventArgs e)
        {
            flexChart1.LegendToggle = chbLegendToggle.Checked;
        }

        private void InitializeControls()
        {
            #region Init controls

            //Get localized description
            baseSample1.lblTitle.Text = Localizer.GetItem("binding", "title");
            baseSample1.tbDescription.Rtf = Localizer.GetItem("binding", "description").MakeRtf();
            flexChart1 = baseSample1.flexChart1;

            // Chart types
            cbChartType = new ComboBox()
            {
                Location = new Point(40, 5),
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray
            };
            cbChartType.Items.Add("Chart type: Line");
            cbChartType.Items.Add("Chart type: LineSymbols");
            cbChartType.Items.Add("Chart type: Area");
            cbChartType.SelectedIndexChanged += cbChartType_SelectedIndexChanged;
            cbChartType.SelectedIndex = 0;

            //InterpolateNulls
            chbInterpolateNulls = new CheckBox()
            {
                Text = "InterpolateNulls",
                AutoSize = true,
                FlatStyle = FlatStyle.Flat,
                Checked = false,
                TextAlign = ContentAlignment.MiddleLeft
            };
            chbInterpolateNulls.CheckedChanged += chbInterpolateNulls_CheckedChanged;

            //LegendToggle
            chbLegendToggle = new CheckBox()
            {
                Text = "LegendToggle",
                AutoSize = true,
                FlatStyle = FlatStyle.Flat,
                Checked = false,
                TextAlign = ContentAlignment.MiddleLeft
            };
            chbLegendToggle.CheckedChanged += chbLegendToggle_CheckedChanged;

            baseSample1.pControls.Controls.Add(cbChartType);
            baseSample1.pControls.Controls.Add(chbInterpolateNulls);
            baseSample1.pControls.Controls.Add(chbLegendToggle);
            #endregion
        }

        public class DataItem
        {
            public int? Downloads { get; set; }
            public int? Sales { get; set; }
            public DateTime Date { get; set; }

        }

    }
}
