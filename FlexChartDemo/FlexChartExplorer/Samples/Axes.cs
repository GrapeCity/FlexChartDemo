﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C1.Win.Chart;
using System.Drawing.Drawing2D;
using C1.Chart;
using System.IO;

namespace FlexChartExplorer.Samples
{
    public partial class Axes : UserControl
    {
        int npts = 12;
        Random rnd = new Random();

        public Axes()
        {
            InitializeComponent();
            InitializeControls();

            SetupChart();
        }

        void SetupChart()
        {
            // Create sample data
            var data = new List<DataItem>();
            var dt = DateTime.Today;
            for (var i = 0; i < npts; i++)
            {
                data.Add(new DataItem() 
                { 
                    Time = dt.AddMonths(i), 
                    Precipitation = rnd.Next(30, 100), 
                    Temperature = rnd.Next(7, 20) 
                });
            }
                
            flexChart1.BeginUpdate();
            flexChart1.Series.Clear();

            // 1st series with auxiliary axis
            var series1 = new Series();
            series1.Name = "prec";
            series1.Binding = "Precipitation";
            series1.ChartType = ChartType.Column;
            series1.AxisY = new Axis() 
            { 
                Position = Position.Right, 
                Min = 0,
                Max = 100, 
                Title = "precipitation, mm" 
            };

            // 2nd series
            var series2 = new Series();
            series2.Name =  "t, avg";
            series2.Binding = "Temperature";
            series2.ChartType = ChartType.LineSymbols;

            // Add data to the chart
            flexChart1.Series.Add(series1);
            flexChart1.Series.Add(series2);
            flexChart1.BindingX = "Time";
            flexChart1.DataSource = data;


            flexChart1.ChartType = ChartType.Column;

            // Set axis appearance
            flexChart1.AxisY.Title = "temperature, C";
            flexChart1.AxisY.Min = 0;
            flexChart1.AxisY.MajorGrid = true;

            flexChart1.EndUpdate();
        }

        public class DataItem
        {
            public DateTime Time { get; set; }

            public int Precipitation { get; set; }
            public int Temperature { get; set; }
        }

        private void cbLabelAngle_SelectedIndexChanged(object sender, EventArgs e)
        {
            flexChart1.AxisX.LabelAngle = (cbLabelAngle.SelectedItem as ComboBoxItem).Value;
        }

        private void InitializeControls()
        {
            #region Init controls

            //Get localized description
            baseSample1.lblTitle.Text = Localizer.GetItem("axes", "title");
            baseSample1.tbDescription.Font = new Font(new FontFamily("宋体"), 10);
            baseSample1.tbDescription.Rtf = Localizer.GetItem("axes", "description").MakeRtf();


            flexChart1 = baseSample1.flexChart1;

            // Chart types
            cbLabelAngle = new ComboBox()
            {
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray
            };
            cbLabelAngle.SelectedIndexChanged += cbLabelAngle_SelectedIndexChanged;

            baseSample1.pControls.Controls.Add(cbLabelAngle);
            #endregion
            foreach (var item in new[]{-90, -45, 0, 45, 90})
                cbLabelAngle.Items.Add(new ComboBoxItem("X Label angle") { Text = item.ToString(), Value = (int)item });
            cbLabelAngle.SelectedIndex = 2;

        }
    }
}
