﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C1.Win.Chart;
using System.Drawing.Drawing2D;

namespace FlexChartExplorer.Samples
{
    public partial class HitTest : UserControl
    {
        const int SERIES_NUMBER = 2;

        public HitTest()
        {
            InitializeComponent();
            InitializeControls();

            SetupChart();
        }

        void SetupChart()
        {
            flexChart1.BeginUpdate();

            flexChart1.Series.Clear();

            // Create sample data and add it to the chart
            var offset = 0;
            for (int i = 0; i < SERIES_NUMBER; i++)
            {
                Series tbs = new Series()
                {
                    Name = "Series " + i.ToString(),
                    DataSource = DataCreator.Create(x => Math.Sin(x + offset), 0, 60, 7),
                    Binding = "YVals",
                    BindingX = "XVals"
                };

                flexChart1.ChartType = C1.Chart.ChartType.SplineSymbols;

                flexChart1.Series.Add(tbs);
                offset += 5;
            }

            // Add header and footer
            var titleFont = new System.Drawing.Font(FontFamily.GenericSansSerif, 15); 
            flexChart1.Header.Content = "Chart Header";
            flexChart1.Header.Style.Font = flexChart1.Footer.Style.Font = titleFont;
            flexChart1.Footer.Content = "Chart Footer";
            flexChart1.Header.Style.FillColor = flexChart1.Footer.Style.FillColor = Color.DimGray;


            flexChart1.EndUpdate();
        }

        private void flexChart1_MouseMove(object sender, MouseEventArgs e)
        {
            // Show information about chart element under mouse cursor
            var ht = flexChart1.HitTest(e.Location);
            var result = new StringBuilder();
            result.AppendLine(string.Format("Chart element:{0}", ht.ChartElement));
            if (ht.Series != null)
                result.AppendLine(string.Format("Serie name:{0}", ht.Series.Name));
            if (ht.PointIndex > 0)
                result.AppendLine(string.Format("Point index={0:0}", ht.PointIndex));
            lblPosition.Text = result.ToString();

            var result2 = new StringBuilder();
            if (ht.Distance > 0)
                result2.AppendLine(string.Format("Distance={0:0}", ht.Distance));
            if (ht.X != null)
                result2.AppendLine(string.Format("X={0:0.00}", ht.X));
            if (ht.Y != null)
                result2.AppendLine(string.Format("Y={0:0.00}", ht.Y));
            lblPosition2.Text = result2.ToString();
        }

        private void InitializeControls()
        {
            #region Init controls

            flexChart1 = baseSample1.flexChart1;
            baseSample1.flexChart1.MouseMove += flexChart1_MouseMove;
            baseSample1.lblTitle.Text = Localizer.GetItem("hittest", "title");
            baseSample1.tbDescription.Rtf = Localizer.GetItem("hittest", "description").MakeRtf();
            baseSample1.pDescription.Height = 100;
            baseSample1.pControls.Visible = false;
            pInformation.Paint += delegate { pInformation.CreateGraphics().DrawLine(new System.Drawing.Pen(System.Drawing.Color.LightGray), 0, 0, pInformation.Width, 0); };

            #endregion
        }
    }
}
