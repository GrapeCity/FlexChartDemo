﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C1.Win.Chart;
using System.Drawing.Drawing2D;

namespace FlexChartExplorer.Samples
{
    public partial class Labels : UserControl
    {
        public Labels()
        {
            InitializeComponent();
            InitializeControls();

            SetupChart();
        }
        void SetupChart()
        {
            flexChart1.BeginUpdate();

             flexChart1.Series.Clear();

            // Add data to the chart
            var s1 = new Series();
            s1.Binding = s1.Name = "March";
            flexChart1.Series.Add(s1);

            var s2 = new Series();
            s2.Binding = s2.Name = "April";
            flexChart1.Series.Add(s2);

            var s3 = new Series();
            s3.Binding = s3.Name = "May";
            flexChart1.Series.Add(s3);

            flexChart1.BindingX = "Fruit";
            flexChart1.DataSource = DataCreator.CreateFruit();

            // Add data labels
            flexChart1.DataLabel.Style.Font = new System.Drawing.Font(FontFamily.GenericSansSerif, 10);
            flexChart1.DataLabel.Position = C1.Chart.LabelPosition.Top;
            flexChart1.DataLabel.Content = "{y}";

            flexChart1.EndUpdate();
        }

        private void cbChartType_SelectedIndexChanged(object sender, EventArgs e)
        {
            flexChart1.ChartType = (C1.Chart.ChartType)Enum.Parse(typeof(C1.Chart.ChartType), (cbChartType.SelectedItem as ComboBoxItem).Text);
        }


        private void cbLabelPosition_SelectedIndexChanged(object sender, EventArgs e)
        {
            flexChart1.DataLabel.Position = (C1.Chart.LabelPosition)Enum.Parse(typeof(C1.Chart.LabelPosition), (cbLabelPosition.SelectedItem as ComboBoxItem).Text);
        }

        private void chbLabelBorder_CheckedChanged(object sender, EventArgs e)
        {
            flexChart1.DataLabel.Border = chbLabelBorder.Checked;
        }

        private void InitializeControls()
        {
            #region Init controls

            //Set localized text
            baseSample1.lblTitle.Text = Localizer.GetItem("labels", "title");
            baseSample1.tbDescription.Rtf = Localizer.GetItem("labels", "description").MakeRtf();

            flexChart1 = baseSample1.flexChart1;

            //Init ChartType combobox
            cbChartType = new ComboBox()
            {
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray
            };
            cbChartType.SelectedIndexChanged += cbChartType_SelectedIndexChanged;

            //Init Palette combobox
            cbLabelPosition = new ComboBox()
            {
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray
            };
            cbLabelPosition.SelectedIndexChanged += cbLabelPosition_SelectedIndexChanged;

            //Init Rotete chackbox
            chbLabelBorder = new CheckBox()
            {
                Text = "Label border",
                FlatStyle = FlatStyle.Flat,
                Checked = false
            };
            chbLabelBorder.CheckedChanged += chbLabelBorder_CheckedChanged;

            baseSample1.pControls.Controls.Add(cbChartType);
            baseSample1.pControls.Controls.Add(cbLabelPosition);
            baseSample1.pControls.Controls.Add(chbLabelBorder);
            #endregion

            foreach (var item in Enum.GetValues(typeof(C1.Chart.ChartType)))
                if (item.ToString() != "Bubble" && item.ToString() != "Candlestick" && item.ToString() != "HighLowOpenClose")
                cbChartType.Items.Add(new ComboBoxItem("Chart type") { Text = ((C1.Chart.ChartType)item).ToString(), Value = (int)item });
            cbChartType.SelectedIndex = 0;

            foreach (var item in Enum.GetValues(typeof(C1.Chart.LabelPosition)))
                cbLabelPosition.Items.Add(new ComboBoxItem("Label position") { Text = ((C1.Chart.LabelPosition)item).ToString(), Value = (int)item });
            cbLabelPosition.SelectedIndex = 2;
        }
    }
}
