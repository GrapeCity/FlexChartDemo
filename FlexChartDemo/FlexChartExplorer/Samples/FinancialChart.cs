﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C1.Win.Chart;
using System.Drawing.Drawing2D;
using C1.Chart;
using System.IO;

namespace FlexChartExplorer.Samples
{
    public partial class FinancialChart : UserControl
    {
        int npts = 100;
        Random rnd = new Random();

        public FinancialChart()
        {
            InitializeComponent();
            InitilizeControls();
            
            SetupChart();
        }

        void SetupChart()
        {
            // Create sample financial data
            var data = new List<Quote>();
            var dt = DateTime.Today;
            for (var i = 0; i < npts; i++)
            {
                var q = new Quote();

                q.Time = dt.AddDays(i);

                if (i > 0)
                    q.Open = data[i - 1].Close;
                else
                    q.Open = 1000;

                q.High = q.Open + rnd.Next(50);
                q.Low = q.Open - rnd.Next(50);

                q.Close = rnd.Next((int)q.Low, (int)q.High);

                q.Volume = rnd.Next(0, 100);
                data.Add(q);
            }

            flexChart1.BeginUpdate();
            
            flexChart1.Series.Clear();

            // 1st series - Stock quotes
            var series1 = new Series();
            series1.Binding = "High,Low,Open,Close";
            series1.BindingX = "Time";
            series1.Name = "price";

            // 2nd series - Volume
            var series2 = new Series();
            series2.Name = series2.Binding = "Volume";
            series2.BindingX = "Time";
            series2.ChartType = ChartType.Column;
            series2.AxisY = new Axis() { Position = Position.Right, Max = 200 };

            // Add data to the chart
            flexChart1.Series.Add(series2);
            flexChart1.Series.Add(series1);
            flexChart1.DataSource = data;

            flexChart1.ChartType = ChartType.Candlestick;
            flexChart1.AxisY.Min = 500;

            flexChart1.EndUpdate();
        }

        public class Quote
        {
            public DateTime Time { get; set; }

            public double Open { get; set; }
            public double Close { get; set; }
            public double High { get; set; }
            public double Low { get; set; }

            public double Volume { get; set; }
        }

        private void InitilizeControls()
        {
            flexChart1 = baseSample1.flexChart1;
            baseSample1.lblTitle.Text = Localizer.GetItem("financialchart", "title");
            baseSample1.tbDescription.Rtf = Localizer.GetItem("financialchart", "description").MakeRtf();

            // Chart types
            cbChartType = new ComboBox()
            {
                Size = new Size(200, 21),
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray
            };
            cbChartType.Items.Add("Chart type: Candlestick");
            cbChartType.Items.Add("Chart type: HighLowOpenClose");
            cbChartType.SelectedIndexChanged += cbChartType_SelectedIndexChanged;
            cbChartType.SelectedIndex = 0;

            btnSaveImage = new Button()
            {
                Text = "Save image",
                Width = 100,
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.DimGray,
                AutoSize = true
            };
            btnSaveImage.Click += btnSaveImage_Click;

            baseSample1.pControls.Controls.Add(cbChartType);
            baseSample1.pControls.Controls.Add(btnSaveImage);
        }

        void btnSaveImage_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                using (var fs = saveFileDialog1.OpenFile())
                {
                    var ext = Path.GetExtension(saveFileDialog1.FileName);
                    var fmt = ImageFormat.Jpg;
                    switch (ext)
                    {
                        case ".png":
                            fmt = ImageFormat.Png;
                            break;
                        case ".svg":
                            fmt = ImageFormat.Svg;
                            break;
                        default:
                            break;
                    }
                    flexChart1.SaveImage(fs, fmt, flexChart1.Width, flexChart1.Height);
                }
            }
        }

        private void cbChartType_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cbChartType.SelectedItem.ToString())
            {
                case "Chart type: Candlestick": flexChart1.ChartType = C1.Chart.ChartType.Candlestick; break;
                case "Chart type: HighLowOpenClose": flexChart1.ChartType = C1.Chart.ChartType.HighLowOpenClose; break;
            }
        }

    }
}
