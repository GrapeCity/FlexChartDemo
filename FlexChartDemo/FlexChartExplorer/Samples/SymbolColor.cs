﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C1.Win.Chart;
using System.Drawing.Drawing2D;

namespace FlexChartExplorer.Samples
{
    public partial class SymbolColor : UserControl
    {
        public SymbolColor()
        {
            InitializeComponent();
            InitializeControls();

            SetupChart();
        }

        void SetupChart()
        {
            flexChart1.BeginUpdate();

            flexChart1.ChartType = C1.Chart.ChartType.Scatter;
            flexChart1.Series.Clear();

            var ser = new Series()
            {
                DataSource = DataCreator.Create(x => Math.Sin(0.1 * x), y => Math.Cos(0.15 * y), 360),
                Binding = "YVals",
                BindingX = "XVals",
            };
            ser.SymbolRendering += (sender, args) =>
                {
                    var y = (double)((DataRowView)args.Item)["YVals"];
                    var r = y >= 0 ? 255 : (255 * (1+ y));
                    var b = y < 0 ? 255 : (255 * (1 - y));
                    var g = (1 - Math.Abs(y)) * 255;
                    args.Engine.SetFill( Color.FromArgb( (int)r, (int)g, (int)b).ToArgb() );
                    args.Engine.SetStroke( null);
                };
            flexChart1.Series.Add(ser);

            flexChart1.EndUpdate();
        }
        private void InitializeControls()
        {
            #region Init controls
            baseSample1.lblTitle.Text = Localizer.GetItem("SymbolColor", "title");
            baseSample1.tbDescription.Rtf = Localizer.GetItem("SymbolColor", "description").MakeRtf();

            flexChart1 = baseSample1.flexChart1;
            baseSample1.pDescription.Height = 160;

            flexChart1.AxisX.MajorGrid = false;
            flexChart1.ChartType = C1.Chart.ChartType.Line;
            baseSample1.pControls.Visible = false;

            #endregion
        }
    }
}
