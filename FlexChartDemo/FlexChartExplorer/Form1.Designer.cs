﻿namespace FlexChartExplorer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panelMain1 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDown = new System.Windows.Forms.Button();
            this.btnVedio = new System.Windows.Forms.Button();
            this.btnPurchse = new System.Windows.Forms.Button();
            this.btnForum = new System.Windows.Forms.Button();
            this.btnDoc = new System.Windows.Forms.Button();
            this.panelMain = new System.Windows.Forms.Panel();
            this.panelLinks = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1.SuspendLayout();
            this.panelMain1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.panelMain1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panelLinks, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(909, 606);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panelMain1
            // 
            this.panelMain1.Controls.Add(this.panel1);
            this.panelMain1.Controls.Add(this.panelMain);
            this.panelMain1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain1.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.panelMain1.Location = new System.Drawing.Point(200, 0);
            this.panelMain1.Margin = new System.Windows.Forms.Padding(0);
            this.panelMain1.Name = "panelMain1";
            this.panelMain1.Size = new System.Drawing.Size(709, 606);
            this.panelMain1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(164)))), ((int)(((byte)(250)))));
            this.panel1.Controls.Add(this.btnDown);
            this.panel1.Controls.Add(this.btnVedio);
            this.panel1.Controls.Add(this.btnPurchse);
            this.panel1.Controls.Add(this.btnForum);
            this.panel1.Controls.Add(this.btnDoc);
            this.panel1.Location = new System.Drawing.Point(399, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 3, 0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(310, 80);
            this.panel1.TabIndex = 0;
            // 
            // btnDown
            // 
            this.btnDown.BackColor = System.Drawing.Color.Transparent;
            this.btnDown.BackgroundImage = global::FlexChartExplorer.Properties.Resources.icon_download;
            this.btnDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDown.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDown.FlatAppearance.BorderSize = 0;
            this.btnDown.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnDown.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDown.Location = new System.Drawing.Point(191, -3);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(61, 72);
            this.btnDown.TabIndex = 2;
            this.btnDown.UseVisualStyleBackColor = false;
            // 
            // btnVedio
            // 
            this.btnVedio.BackColor = System.Drawing.Color.Transparent;
            this.btnVedio.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnVedio.BackgroundImage")));
            this.btnVedio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnVedio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVedio.FlatAppearance.BorderSize = 0;
            this.btnVedio.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnVedio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnVedio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVedio.Location = new System.Drawing.Point(65, -1);
            this.btnVedio.Name = "btnVedio";
            this.btnVedio.Size = new System.Drawing.Size(70, 69);
            this.btnVedio.TabIndex = 2;
            this.btnVedio.UseVisualStyleBackColor = false;
            // 
            // btnPurchse
            // 
            this.btnPurchse.BackColor = System.Drawing.Color.Transparent;
            this.btnPurchse.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPurchse.BackgroundImage")));
            this.btnPurchse.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPurchse.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPurchse.FlatAppearance.BorderSize = 0;
            this.btnPurchse.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnPurchse.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnPurchse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPurchse.Location = new System.Drawing.Point(241, -3);
            this.btnPurchse.Name = "btnPurchse";
            this.btnPurchse.Size = new System.Drawing.Size(84, 75);
            this.btnPurchse.TabIndex = 2;
            this.btnPurchse.UseVisualStyleBackColor = false;
            // 
            // btnForum
            // 
            this.btnForum.BackColor = System.Drawing.Color.Transparent;
            this.btnForum.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnForum.BackgroundImage")));
            this.btnForum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnForum.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnForum.FlatAppearance.BorderSize = 0;
            this.btnForum.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnForum.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnForum.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnForum.Location = new System.Drawing.Point(122, -4);
            this.btnForum.Name = "btnForum";
            this.btnForum.Size = new System.Drawing.Size(78, 72);
            this.btnForum.TabIndex = 2;
            this.btnForum.UseVisualStyleBackColor = false;
            // 
            // btnDoc
            // 
            this.btnDoc.BackColor = System.Drawing.Color.Transparent;
            this.btnDoc.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDoc.BackgroundImage")));
            this.btnDoc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDoc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDoc.FlatAppearance.BorderSize = 0;
            this.btnDoc.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnDoc.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnDoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDoc.Location = new System.Drawing.Point(0, 0);
            this.btnDoc.Name = "btnDoc";
            this.btnDoc.Size = new System.Drawing.Size(71, 68);
            this.btnDoc.TabIndex = 2;
            this.btnDoc.UseVisualStyleBackColor = false;
            // 
            // panelMain
            // 
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(0, 0);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(709, 606);
            this.panelMain.TabIndex = 1;
            // 
            // panelLinks
            // 
            this.panelLinks.BackColor = System.Drawing.Color.White;
            this.panelLinks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLinks.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.panelLinks.Location = new System.Drawing.Point(0, 0);
            this.panelLinks.Margin = new System.Windows.Forms.Padding(0);
            this.panelLinks.Name = "panelLinks";
            this.panelLinks.Size = new System.Drawing.Size(200, 606);
            this.panelLinks.TabIndex = 1;
            this.panelLinks.Paint += new System.Windows.Forms.PaintEventHandler(this.panelLinks_Paint);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(909, 606);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ForeColor = System.Drawing.Color.Gray;
            this.Name = "MainForm";
            this.Text = "FlexChart浏览器";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panelMain1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panelMain1;
        private System.Windows.Forms.Panel panelLinks;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnPurchse;
        private System.Windows.Forms.Button btnForum;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.Button btnDoc;
        private System.Windows.Forms.Button btnVedio;
    }
}

