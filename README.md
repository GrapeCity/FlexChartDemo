#FlexChart-Winform-Demo

FlexChart是快速灵活的.NET图表控件，追求卓越的Excel图表类型。多种可定制的图表类型满足您的数据可视化需求，使用内置的图表元素丰富你的图表。这个演示针对Winfrom平台。

官网地址：http://www.gcpowertools.com.cn/products/componentone_studio_enterprise.htm

下载链接：http://www.gcpowertools.com.cn/products/download.aspx?pid=2

在线演示：http://www.gcpowertools.com.cn/products/componentone_studio_winform_demo.htm

中文社区：http://gcdn.gcpowertools.com.cn/

![葡萄城控件微信服务号](http://www.gcpowertools.com.cn/newimages/qrgrapecity.png "葡萄城控件微信服务号")
